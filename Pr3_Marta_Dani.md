ACTIVITAT Storage Engines MySQL


ENUNCIAT

Partint d'una màquina CentOS 7 amb el Percona Server 5.7 instal·lat realitza els següents apartats a on es tracten els diferents Storage Engines que conté el MySQL i en conseqüència el Percona Server.

LLIURAMENT

L'entrega d'aquesta activitat correspont a la documentació de tots els passos corresponents a la instal·lació per tenir un SGBD MySQL Percona en funcionament.
En la documentació cal incloure les captures d'imatges que es cregui convenients així com la webgrafia utilitzada.

Opcions de lliurament:
•	Utilitzar aquest document com a plantilla per l’entrega de l’activitat i penjar-la en tasca destinada a aquesta activitat dins del curs Moodle
•	Realitzar la documentació sobre un repositori GIT (Bitbucket o GitLab). Utilitzant el format de fitxer MarkDown (MD). (1 punt)
•	Dins d'una WikiMedia. (1 punt)

Per les dues darreres opcions de lliurament caldrà indicar en el document penjat en el Moodle la URL a on es troba la documentació.
 
Activitat 1. REALITZA I/O RESPON ELS SEGÜENTS APARTATS (obligatòria) (1 punts)

•	Indica quins són els motors d’emmagatzematge que pots utilitzar (quins estan actius)? Mostra al comanda utilitzada i el resultat d’aquesta.****


![img](./imatges/ex1-1.png)
 
•	Com puc saber quin és el motor d’emmagatzematge per defecte. Mostra com canviar aquest 
paràmetre de tal manera que les noves taules que creem a la BD per defecte utilitzin el motor MyISAM?

![img](./imatges/ex1-2.png)

**El que te marcat suport per defecte és InnoDB**
 
**/etc/my.cnf**
**Reiniciem el mysql**

**Comprovem que ha canviat**

![img](./imatges/ex1-3.png)
 
•	Explica els passos per instal·lar i activar l'ENGINE MyRocks. MyRocks és un motor d'emmagatzematge per MySQL basat en RocksDB (SGBD incrustat de tipus clau-valor). Aquest tipus d’emmagatzematge està optimitzat per ser molt eficient en les escriptures amb lectures acceptables.
https://www.percona.com/doc/percona-server/LATEST/myrocks/install.html

**sudo yum install Percona-Server-rocksdb-57.x86_64`**
 
![img](./imatges/ex1-4.png)

![img](./imatges/ex1-5.png)

**sudo ps-admin –enable-rocksdb -u root -pp@st@n@g@**

![img](./imatges/ex1-6.png)
 
**fem un show engines per veure que el tenim.**
 
![img](./imatges/ex1-7.png)

Checkpoint: Mostra al professor que està instal·lat i posa un exemple de com funciona.


•	Importa la BD Sakila com a taules MyISAM. Fes els canvis necessaris per importar la BD Sakila perquè totes les taules siguin de tipus MyISAM.
Mira quins són els fitxers físics que ha creat, quan ocupen i quines són les seves extensions. Mostra'n una captura de pantalla i indica què conté cada fitxer.
 
 ![img](./imatges/ex1-8.png)
 
 ![img](./imatges/ex1-9.png)
 
 
Un cop fet això torna a deixar el motor InnoDB per defecte.

![img](./imatges/ex1-10.png)


 
•	A partir de MySQL apareixen els schemas de metadades i informació guardats amb InnoDB. Busca informació d'aquests schemas. Indica quin és l'objectiu de cadascun d'ells i posa'n un exemple d'ús.
HACER

![img](./imatges/ex1-11.png)

**• INNODB_SYS_TABLES proporciona metadades sobre taules InnoDB, equivalents a la informació de la taula SYS_TABLES del diccionari de dades InnoDB.**

**• INNODB_SYS_COLUMNS proporciona metadades sobre columnes de taula InnoDB, equivalents a la informació de la taula SYS_COLUMNS del diccionari de dades InnoDB.**

**• INNODB_SYS_INDEXES proporciona metadades sobre índexs InnoDB, equivalent a la informació de la taula SYS_INDEXES del diccionari de dades InnoDB.**

**• INNODB_SYS_FIELDS proporciona metadades sobre les columnes (camps) clau dels índexs InnoDB, equivalent a la informació de la taula SYS_FIELDS del diccionari de dades InnoDB.**

**• INNODB_SYS_TABLESTATS proporciona una vista d’informació d’estat de baix nivell sobre les taules InnoDB que es deriven d’estructures de dades a la memòria. No hi ha una taula de sistema interna corresponent InnoDB.**

**• INNODB_SYS_DATAFILES proporciona informació de ruta del fitxer de dades dels espais de taules de fitxer InnoDB, equivalents a la informació de la taula SYS_DATAFILES del diccionari de dades InnoDB.**

**• INNODB_SYS_TABLESPACES proporciona metadades sobre els espais de taules de fitxer per taula de InnoDB, equivalents a la informació de la taulaSYS_TABLESPACES del diccionari de dades InnoDB.**

**• INNODB_SYS_FOREIGN proporciona metadades sobre claus estrangeres definides a les taules InnoDB, equivalents a la informació de la taula SYS_FOREIGN del diccionari de dades InnoDB.**

**• INNODB_SYS_FOREIGN_COLS proporciona metadades sobre les columnes de claus estrangeres que es defineixen a les taules InnoDB, equivalents a la informació del quadre SYS_FOREIGN_COLS del diccionari de dades InnoDB.**

•	Posa un exemple que produeix un DEADLOCK i mostra-ho al professor.
https://stackoverflow.com/questions/31552766/how-to-cause-deadlock-on-mysql



Activitat 2. INNODB part I. REALITZA ELS SEGÜENTS APARTATS (obligatòria) (2 punts)


•	Desactiva l’opció que ve per defecte de innodb_file_per_table
 
![img](./imatges/ex2-1.png) 

**escriurem la següent línia en el fitxer /etc/my.cnf i ficarem de valor 0.**

•	Importa la BD Sakila com a taules InnoDB.
 
![img](./imatges/ex2-2.png) 

![img](./imatges/ex2-3.png)

•	Quin/quins són els fitxers de dades? A on es troben i quin és la seva mida?

![img](./imatges/ex2-4.png)

**Podem trobar els fitxers de dades mirant la variable @@datadir , en dirà la ruta on s´emmagatzemen aquests.**

![img](./imatges/ex2-5.png)

**Aqui podem trobar les dades de la base de dades sakila.**

•	Canvia la configuració del MySQL per:

•	Canviar la localització dels fitxers del tablespace de sistema per defecte a /discs-mysql/

**Crearem la carpeta i li ficarem propietats i permisos, i copiarem tot el contingut del datadir original a la nova ruta.**

**sudo mkdir /discs-mysql**

**chown -R mysql:mysql /discs-mysql**

**cp -R -p /var/lib/mysql/* / discs-mysql**

**semanage fcontext -a -t mysqld_db_t "/discs-mysql(/.*)?"**

**restorecon -R / discs-mysql**

![img](./imatges/ex2-6.png)

•	Tinguem dos fitxers corresponents al tablespace de sistema.


•	Tots dos han de tenir la mateixa mida inicial (10MB)

•	El tablespace ha de créixer de 5MB en 5MB.
•	Situa aquests fitxers (de manera relativa a la localització per defecte) en una nova localització simulant el següent:

 ![img](./imatges/ex2-7.png)

**Editarem el fitxer    /etc/my.cnf i afegirem les següents  línies**

El parametre innodb_data_home_dir si esta buit agafarà per defecte el valor del datadir 
•	/discs-mysql/disk1/primer fitxer de dades → simularà un disc dur
•	/discs-mysql/disk2/segon fitxer de dades → simularà un segon disc dur.
•	(0,5 punts) si les dues rutes anteriors són dos discs físics diferents.
 
 ![img](./imatges/ex2-9.png) 
 
![img](./imatges/ex2-8.png) 

![img](./imatges/ex2-11.png) 

Checkpoint: Mostra al professor els canvis realitzats i que la BD continua funcionant.


Activitat 3. INNODB part II. REALITZA ELS SEGÜENTS APARTATS (obligatòria) (1 punt)


•	Partint de l'esquema anterior configura el Percona Server perquè cada taula generi el seu propi tablespace en una carpeta anomenada tspaces (aquesta pot estar situada a on vulgueu).
•	Indica quins són els canvis de configuració que has realitzat.

**Activarem la variable innodb_file_per_table ficant la instrucció que es veu a la captura o podem modificar el fitxer /etc/my.cnf i canviar el valor d´aquesta.**

![img](./imatges/ex3.1.jpg)

**Comprovarem que estigui activat.**

![img](./imatges/ex3.2.jpg)

**Crearem la carpeta tspaces amb propietats i permisos a la ruta que vulguem.**

 
![img](./imatges/ex3.3.jpg)

![img](./imatges/ex3.4.jpg)
 
**Copiarem tot el contingut de la carpeta a la nova, farem un semanage i un restorecon.**
**Reiniciarem el servei i verificarem que la ruta ha canviat.**

![img](./imatges/ex3.5.jpg)

Activitat 4. INNODB part III. REALITZA ELS SEGÜENTS APARTATS (obligatòria) (1 punt)


•	Crea un tablespace anomenat 'ts1' situat a /discs-mysql/disc1/ i col·loca les taules actor, address i category de la BD Sakila.
  
![img](./imatges/ex4.1.jpg)

![img](./imatges/ex4.2.jpg)
 
•	Crea un altre tablespace anomenat 'ts2' situat a /discs-mysql/disc2/ i col·loca-hi la resta de taules.
![img](./imatges/ex4.3.jpg)

![img](./imatges/ex4.4.jpg)

![img](./imatges/ex4.5.jpg)
 
 
•	Comprova que pots realitzar operacions DML a les taules dels dos tablespaces.
•	Quines comandes i configuracions has realitzat per fer els dos apartats anteriors?

  ![img](./imatges/ex4.6.jpg)
  
  ![img](./imatges/ex4.7.jpg)
  
  ![img](./imatges/ex4.8.jpg)
  
  ![img](./imatges/ex4.9.jpg)
 
 
 


Checkpoint: Mostra al professor els canvis realitzats i que la BD continua funcionant


Activitat 5. REDOLOG. REALITZA ELS SEGÜENTS APARTATS. (2 punt)


•	Com podem comprovar (Innodb Log Checkpointing):
•	LSN (Log Sequence Number)
•	L'últim LSN actualitzat a disc

**Escriurem : SHOW ENGINES INNODB STATUS\G**

![img](./imatges/ex5.1.jpg)
 
**L´ultim LSN actualitzat a disc és 3743968**

•	Quin és l'últim LSN que se li ha fet Checkpoint

**L´ultim LSN que se li ha fet Checkpoint és 3743959**

•	Proposa un exemple a on es vegi l'ús del redolog

**https://docs.oracle.com/cd/B19306_01/server.102/b14231/onlineredo.htm**

•	Com podem mirar el número de pàgines modificades (dirty pages)? I el número total de pàgines?

![img](./imatges/ex5.2.jpg)

Checkpoint: Mostra al professor els canvis realitzats i que la BD continua funcionant.
 
Activitat 6. Implentar BD Distribuïdes. (1,5 punts)


Com s'ha vist a classe MySQL proporciona el motor d'emmagatzemament FEDERATED que té com a funció permetre l'accés remot a bases de dades MySQL en un servidor local sense utilitzar tècniques de replicació ni clustering.

•	Prepara un Servidor Percona Server amb la BD de Sakila
•	Prepara un segon servidor Percona Server a on hi hauran un conjunt de taules
FEDERADES al primer servdor.
•	Per realitzar aquest link entre les dues BD podem fer-ho de dues maneres:
•	Opció1: especificar TOTA la cadena de connexió a CONNECTION
•	Opció2: especifficar una connexió a un server a CONNECTION que prèviament s'ha creat mitjançant CREATE SERVER
•	Posa un exemple de 2 taules de cada opció.
Tingues en compte els permisos a nivell de BD i de SO així com temes de seguretat com firewalls, etc...
•	Detalla quines són els passos i comandes que has hagut de realitzar en cada màquina.


Checkpoint: Mostra al professor la configuració que has hagut de realitzar i el seu funcionament.

Activitat 7. Storage Engine CSV (0,5 punts)


•	Documenta i posa exemple de com utilitzar ENGINE CSV.
•	Cal documentar els passos que has hagut de realitzar per preparar l'exemple: configuracions, instruccions DML, DDL, etc....

**CREATE TABLE test (i INT NOT NULL, c CHAR(10) NOT NULL)**

**ENGINE = CSV;**

**INSERT INTO test VALUES(1,'record one'),(2,'record two');**

**SELECT * FROM test;**

![img](./imatges/ex7.1.jpg)
 
 
![img](./imatges/ex7.2.jpg)


Checkpoint: Mostra al professor la configuració que has hagut de realitzar i el seu funcionament.

Activitat 8. Storage Engine MyRocks (1 punt)


•	Documenta i posa exemple de com utilitzar ENGINE MyRocks. Crea una Base de dades amb 2 o 3 taules i inserta-hi contingut.
•	Cal documentar els passos que has hagut de realitzar per preparar l'exemple: configuracions, instruccions DML, DDL, etc....
 
•	A quin directori es guarden els fitxers de dades? Fes un llistat de a on són els fitxers i què ocupen.
•	Quina és la compressió per defecte que utilitza per les taules? Com ho faries per canviar- lo. Per exemple utilitza Zlib o ZSTD o sense compressió.

Checkpoint: Mostra al professor la configuració que has hagut de realitzar i el seu funcionament.
